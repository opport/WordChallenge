package wordchallenge;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import static wordchallenge.GameFunctions.*;

/**
 * A Game to test one's vocabulary.
 *
 * <pre>
 * The player gains points by using a number of random letters to form words with in a time frame.
 * Words are taken from dwyl's gitproject for english-words.
 * <ul>
 *      <li>The valid word length is a minimum of 2 randomLetters and a variable maximum value depending on user configuration with a default of 7.</li>
 *      <li>Points are awarded based on word length and, optionally, word variety and randomLetters used.</li>
 *      <li>The player can choose to generate new randomLetters at the cost of a few seconds of game time.</li>
 *      <li>Time is counted down from a default of 2 minutes and can be configured by the user.</li>
 *      <li>Bonus: Multiple correct words grant progressively increasing bonus points up to a cap.</li>
 *      <li>Bonus: Multiple correct words grant progressively increasing bonus seconds up to a cap.</li>
 *  </ul>
 * </pre>
 *
 * @author PR
 * @version 1.0
 * @see <a href="https://github.com/dwyl/english-words/">dwyl english-words</a>
 */
public class WordChallenge extends Application {

    /**
     * Game settings/preferences to be loaded for this class.
     */
    static Preferences preferences = Preferences.userNodeForPackage(wordchallenge.WordChallenge.class);

    /**
     * Choice to choose scrabble letter values.
     */
    static final boolean USE_SCRABBLE_SCORE_DEFAULT = false;
    /**
     * Default Path of textfile that contains wordlist.
     */
    static final String DEFAULT_TEXTFILE_PATH = new File("").getAbsolutePath();
    /**
     * User defined path of textfile that contains wordlist.
     */
    static String textFilePath;
    /**
     * File with wordlist.
     */
    static File textFile = new File(DEFAULT_TEXTFILE_PATH);
    /**
     * Default Name to assign highscores to.
     */
    static final String DEFAULT_USERNAME = "Name";
    /**
     * Max name length.
     */
    static final int MAX_NAME_LENGTH = 16;
    /**
     * Minimum number of randomLetters for a word.
     */
    static final int MIN_LETTERS = 2;
    /**
     * Default maximum number of randomLetters for a word.
     */
    static final int MAX_LETTERS_DEFAULT = 7;
    /**
     * User configured maximum number of randomLetters for a word.
     */
    static int MAX_LETTERS;
    /**
     * User defined name to assign highscores to.
     */
    private static String username;
    /**
     * List to save highscores.
     */
    static final List<Integer> highscore = new ArrayList<>();

    /**
     * Load preferences using default values.
     *
     * @param stage
     * @throws Exception
     */
    private static void setPreferences() {
        preferences.putBoolean("USE_SCRABBLE_SCORE", USE_SCRABBLE_SCORE_DEFAULT);
        preferences.put("TEXTFILE_PATH", DEFAULT_TEXTFILE_PATH);
        preferences.putInt("MAX_LETTERS", MAX_LETTERS_DEFAULT);
        preferences.putInt("MIN_LETTERS", MIN_LETTERS);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        setPreferences();
        extractWords();
        //gameLoop();
        launch(args);
        System.exit(0);
    }

}
