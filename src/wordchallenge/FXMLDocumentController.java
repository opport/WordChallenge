package wordchallenge;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static wordchallenge.GameFunctions.gameLoop;

/**
 *
 * @author PR
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    
    @FXML
    private TextField playerNameTextField;
    
    @FXML
    private Button startButton;
    
    @FXML
    private Label playerNameLabel;
    @FXML
    private Label wordLabel;
    @FXML
    private Button settingsButton;
    @FXML
    private Label randomLettersLabel;
    @FXML
    private Button settingsButton1;
    @FXML
    private Label highscoreLabel1;
    @FXML
    private Label pointAmountLabel;
    @FXML
    private TextField wordsTextField;
    
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    /** Start game after clicking the start button
     * <pre>
     * Configuration options are disabled and the game is started.
     * After the game ends, the highscore is updated and the configuration options are enabled again.
     * </pre>
     * @param event start game button is clicked
     */
    @FXML
    private void startGameAction(ActionEvent event) {
        //TODO: Disable configuration options until game end
        playerNameTextField.setDisable(true);
        //wordFileTextField.setDisable(true);
        //wordFileButton.setDisable(true);
        gameLoop();
    }
    
    /** Choose a custom word file to read words from.
     * 
     * @param event button is clicked to initialize
     */
    private void chooseWordFile(ActionEvent event) {
        Stage stage = (Stage)label.getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Open Word File");
        WordChallenge.preferences.put("TEXTFILE_PATH", fileChooser.showOpenDialog(stage).getAbsolutePath());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        playerNameTextField.setText(WordChallenge.preferences.get("USERNAME",WordChallenge.DEFAULT_USERNAME));
        //wordFileTextField.setText(WordChallenge.preferences.get("TEXTFILE_PATH", WordChallenge.DEFAULT_TEXTFILE_PATH));
    }    
    
}
