package wordchallenge;

import java.awt.event.KeyEvent;
import static wordchallenge.GameFunctions.randomLetters;

/**
 * The handler object that makes sure the input matches the possible letters or
 * special settings keys.
 *
 * @author PR
 * @version 1.0
 */
public class KeyHandler {

    /**
     * Filters out invalid input.<br>
     * <pre>
     * Invalid keys include:
     * 1. Input that does not match possible letters from current random word.
     * 2. Input that does not match special settings keys.
     * 3. Copy pasting.
     * </pre>
     *
     * @param evt Key event to be filtered
     */
    public static void filterInput(final KeyEvent evt) {
        if (!randomLetters.contains(evt.getKeyChar())
                || evt.getKeyCode() == KeyEvent.VK_V) {
            evt.consume();
        }
    }
}
