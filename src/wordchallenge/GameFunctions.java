package wordchallenge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.stream.Collectors;
import static wordchallenge.WordChallenge.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class with functions used inside the game loop.
 *
 * @author PR
 * @version Expression revision is undefined on line 12, column 15 in
 * Templates/Classes/Class.java.
 */
public class GameFunctions {

    /**
     * Letters to be generated each at the beginning of a game or when a player.
     * requests to.
     */
    static List<Character> randomLetters;
    /**
     * Timer to show remaining time for input.
     */
    static final Timer gameTimer = new Timer();
    /**
     * Variable to keep track of score in current game.
     */
    static int currentScore = 0;
    /**
     * Score multiplier for multiple valid inputs in a row.
     */
    private static int comboMultiplier = 1;
    /**
     * List of possible Words extracted from file
     */
    static final HashSet<String> validWordsList = new HashSet<>();
    /**
     * List of valid words the user has already entered from file.
     */
    static HashSet enteredWordsList;

    /**
     * Show whether game is currently on.
     */
    static boolean isPlaying = false;

    /**
     * Scrabble letter values for bonus point mode.
     * <pre>
     * (1 point)-A, E, I, O, U, L, N, S, T, R.
     * (2 points)-D, G.
     * (3 points)-B, C, M, P.
     * (4 points)-F, H, V, W, Y.
     * (5 points)-K.
     * (8 points)- J, X.
     * (10 points)-Q, Z
     * </pre>
     */
    //.put(1, {'a', 'e', 'i', 'o', 'u' , 'l', 'n', 's', 't','r'})
    static final HashMap<Character, Integer> letterValues = new HashMap<>(26);

    static {
        letterValues.put('a', 1);
        letterValues.put('e', 1);
        letterValues.put('i', 1);
        letterValues.put('o', 1);
        letterValues.put('u', 1);
        letterValues.put('l', 1);
        letterValues.put('n', 1);
        letterValues.put('s', 1);
        letterValues.put('t', 1);
        letterValues.put('r', 1);
        letterValues.put('d', 2);
        letterValues.put('g', 2);
        letterValues.put('b', 3);
        letterValues.put('c', 3);
        letterValues.put('m', 3);
        letterValues.put('p', 3);
        letterValues.put('f', 4);
        letterValues.put('h', 4);
        letterValues.put('v', 4);
        letterValues.put('w', 4);
        letterValues.put('y', 4);
        letterValues.put('k', 5);
        letterValues.put('j', 8);
        letterValues.put('x', 8);
        letterValues.put('q', 10);
        letterValues.put('z', 10);
    }

    /**
     * Get a list of valid words from a file. All extracted words are made
     * lowercase.
     * <pre>
     * If no file is found, the user is prompted to choose a file to use.
     * </pre>
     */
    static void extractWords() {
        try {
            //TODO use settings to get custom file
            Scanner scanner = new Scanner(WordChallenge.class.getResourceAsStream("words"));
            String line;

            // Actual word extraction
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();

                if (!line.matches("^.*(\\W|\\d).*$")) {    //Contains non alpha
                    validWordsList.add(line.toLowerCase());
                }
            }

        } catch (NullPointerException e) {
            //TODO prompt user for file
            System.err.println(e.getMessage());
        }
    }

    /**
     * Calculate the score for each valid word.
     * <pre>
     * If scrabble score values were enabled, the function returns the sum of the
     * letter values for each input letter found in the letterValues HashMap.
     * </pre>
     *
     * @param input User input to be calculated
     * @param comboMultiplier Score multiplier to be applied to score
     * @return calculated score
     */
    static int calculateScore(final String input, final int comboMultiplier) {
        if (preferences.getBoolean("USE_SCRABBLE_SCORE", USE_SCRABBLE_SCORE_DEFAULT)) {
            int sum = 0;
            for (int i = 0; i < input.length(); i++) {
                sum += letterValues.get(input.charAt(i));
            }
            return sum;
        } else {
            return input.length() * comboMultiplier;
        }
    }

    //TODO make generation truly random by accounting for words that have already been selected.
    /**
     * Generates random combination of randomLetters to be used in game.
     * <pre>
     * The function is called each time a user decides
     * to generate new randomLetters.
     * </pre>
     *
     * @return char array the size of the maximum number of allowed
     * randomLetters for a word
     */
    static List<Character> generateRandomLetters() {
        /**
         * Variable for generating random numbers
         */
        Random random = new Random();
        /**
         * HashSet to filter words based on MAX_LETTERS setting
         */
        List<String> possibleWords = validWordsList.stream()
                .filter(word -> word.length() == MAX_LETTERS_DEFAULT)
                .collect(Collectors.toList());
        /**
         * Integer to get random word from filtered results
         */
        int randomInt = random.nextInt(possibleWords.size());
        /**
         * Random word selected based on random Integer
         */
        String randomWord = possibleWords.get(randomInt);

        List<String> stringList = Arrays.asList(randomWord.split(""));
        randomLetters = stringList.stream()
                .flatMapToInt(String::chars)
                .mapToObj(i -> (char) i)
                .collect(Collectors.toList());

        Collections.shuffle(randomLetters);

        return randomLetters;
    }

    //TODO replace function with a state pattern
    /**
     * Template function to get next word whenever a certain character is
     * entered or a certain button is used in the game to trigger it. <br>
     * First, the next set of letters are obtained by selecting the next word
     * with {@linkplain #generateRandomLetters()}, <br>
     * then previously entered words are cleared, unless the player is playing
     * in hard mode, <br>
     * and , finally, the {@linkplain #comboMultiplier} is reset.
     */
    private static void getNextWord() {
        generateRandomLetters();
        //if (mode != hardmode)
        enteredWordsList.clear();
        comboMultiplier = 1;
    }

    /**
     * Template function granting points for correct input. First, points to be
     * awarded are calculated using
     * {@link #calculateScore(java.lang.String, int) }, <br>
     * then the input is added to the {@link #enteredWordsList}, <br>
     * and, finally, the combo multiplier is increased if it has not reached the
     * cap yet and the player can rejoice over a positively reinforcing message
     * from the game.
     *
     * @param input String to be evaluated for points
     */
    private static void awardPoints(String input) {
        int pointsGained = calculateScore(input, comboMultiplier);
        currentScore += pointsGained;
        enteredWordsList.add(input);

        if (comboMultiplier < MAX_LETTERS_DEFAULT) { // Increase combomultiplier until the cap
            comboMultiplier++;
        }
        System.out.println(input + "= +" + pointsGained + " points! (combo=" + (comboMultiplier - 1) + ")");
    }

    /**
     * Template function to punish wrong input. Player is shamed by receiving a
     * scathing remark and having the bonus multiplier reset.
     */
    private static void punishWrongInput(String input) {
        System.out.println(input + " not valid.");
        comboMultiplier = 1;
    }

    /**
     * Checks if all letters and the amount thereof in the input word are in the
     * list of valid letters.
     * <pre>
     * The function loops each letter of the input word, checking whether it is
     * found in the list of valid letters.
     * Any match removes one instance of the found letter from the list of
     * remaining valid letters.
     * </pre>
     *
     * @param letters array of letters to be checked
     * @return boolean whether a valid amount of all input letters are found in
     * the list of valid letters.
     */
    static boolean inputLettersAreValid(final String letters) {

        List<Character> tempList = new ArrayList(randomLetters);

        for (int i = 0; i < letters.length(); i++) {
            Character tempChar = letters.charAt(i);

            if (tempList.contains(tempChar)) {
                tempList.remove(tempChar);
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * Function to decide what to do with the input letters.
     * <pre>
     *  <ul>
     *      <li> '-' or "" Stops the program.
     *      <li> ' ' (Space) randomizes letters and resets combo multiplier.
     *      <li> MIN_LETTERS < input < MAX_LETTERS is evaluated.
     *      <li> Input outside the valid range is not evaluated.
     *      <li> Correct input is rewarded with points and a higher combo multiplier.
     *      <li> Incorrect input is punished by resetting the combo multiplier.
     *      <li> Previously entered words are not counted and prompt for another try.
     *  </ul>
     * </pre>
     *
     * @param input to be checked
     */
    static void checkInput(final String input) {

        if (input.equals("-") || input.equals("")) { // Exit game if empty string was entered
            isPlaying = false;
        } else if (input.equals(" ")) { // Get next word
            getNextWord();
        } else if (input.length() < MIN_LETTERS || input.length() > MAX_LETTERS_DEFAULT) { // Prompt for correct input
            System.out.println("Please enter word with "
                    + MIN_LETTERS + "-" + MAX_LETTERS + " letters");
        } else if (input.matches("\\d")
                || !inputLettersAreValid(input)
                || !validWordsList.contains(input)) { // Issue #7 Check for correct input // Wrong input, resets combo :(
            punishWrongInput(input);
        } else if (validWordsList.contains(input)
                && !enteredWordsList.contains(input)) {  // Reward correct input
            awardPoints(input);
        } else {
            System.out.println(input + " already entered.");
        }
    }

    /**
     * The loop that prompts the user for input until time is up or the user
     * stops the game.
     * <pre>
     * The function initializes required lists, keeps track of the score
     * and allows the user to generate new randomLetters or quit the game.
     * </pre>
     */
    static void gameLoop() {
        /**
         * Scanner for user input
         */
        Scanner scanner = new Scanner(System.in);
        /**
         * String for storing the user input to evaluate
         */
        String input;

        // Initialize variables and letters
        isPlaying = true;
        enteredWordsList = new HashSet<>();
        generateRandomLetters();

        // Actual game loop
        while (isPlaying) {
            System.out.println("Enter word using letters: " + randomLetters);
            input = scanner.nextLine();

            checkInput(input);
        }

        System.out.println("Final score :" + currentScore);
    }
}
